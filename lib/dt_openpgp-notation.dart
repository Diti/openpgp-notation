part of dt_openpgp;

/**
 * A helper class for creating OpenPGP notation files.
 */
class OpenPgpNotation {
  String date;
  String location;
  OpenPgpKey signer;
  OpenPgpKey signee;

  OpenPgpNotation(this.signer, this.signee, {this.location, this.date})
  {
    if (this.date == null) {
      date = new DateFormat("yyyy-MM-dd").format(new DateTime.now());
    }
  }

  String _output() {
    var buf = new StringBuffer();

    // The standard console grid is, often, 80-column wide.
    String filler = new List.filled(79, "-").join();
    String footerTxt = 'Dimitri Torterat <kra@diti.me>';

    buf ..write('''
$filler
OpenPGP certification notes from key `${signer.longId0x}`
$filler

This digitally-signed document provides details about the OpenPGP certification
process (also known as "key signing") that occured between myself ("Diti") and
the person having their key certificated (the "signee").

More information about my key certification process may be found at this URL:  
http://diti.me/pgp/#policy

  - Diti's fingerprint:    `FD4F 1D56 6452 19A0 C6F6  F9AB 31A4 9121 CD42 FF00`
  - Signee's fingerprint:  `${signee.fingerprint}`
''') // TODO: proper spacing of [OpenPgpKey.fingerprint] at output

        ..writeln('\n### Details ###\n')
        ..writeln('Date:        ${date}  ')
        ..writeln((location == null) ? '' : 'Location:    ${location}  ')

        /*
         * TODO: Add options for a "Notes:" section, like
         *   - How I checked the fingerprint
         *   - Do I know them personally and/or IRL
         *   - Ownership of e-mails (decyphering) verified or not
         *   - Check of government-issued IDs (& photo against person)
         */

        ..writeln('\n$filler\n$footerTxt\n$filler');
    return buf.toString();
  }

  String toString() => _output();

  /**
   * Writes the OpenPGP notation to a file. If no [filename] is given, the
   * script will create a file in project's out/ directory, with "extension"
   * `.notes`, preceeded with the signee's fingerprint.
   *
   * This behavior exists under the assumption that you would use %f.notes.asc
   * in GnuPG. For example, I use the following GnuPG setting in `gpg.conf`:
   *
   *     cert-notation CD42FF00@diti.me=http://diti.me/pgp/certs/%f.notes.asc
   */
  void toFile([String filename]) {
    String path;

    if (filename == null) {
      path = './out/${signee.fingerprint}.notes';
    } else {
      path = filename;
    }

    new Directory(path).parent.createSync(recursive: true);
    new File(Platform.script.resolve(path).toFilePath(windows: Platform.isWindows))
        .open(mode: WRITE).then((RandomAccessFile file) {
          file.writeString(_output()); // TODO: Handle onError
        });
  }
}