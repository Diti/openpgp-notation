part of dt_openpgp;

/**
 * A basic OpenPGP key data representation.
 */
class OpenPgpKey {
  static const int _BITS_SHORTID = 8;
  static const int _BITS_LONGID = 16;
  static const int _BITS_FINGERPRINT = 40;
  static const String _HEX_PREFIX = '0x';
  static const List _VALID_ID_SIZES =
      const [_BITS_SHORTID, _BITS_LONGID, _BITS_FINGERPRINT];

  // The original key ID.
  String _id;

  // The hexadecimal prefix of a key identifier.
  String _hexPrefix;

  /**
   * The 8-char long identifier of the key.
   */
  String shortId;

  /**
   * The 16-char long identifier of the key.
   */
  String longId;

  /**
   * The full fingerprint of the key (30 chars).
   */
  String fingerprint;

  /**
   * Creates a new [OpenPgpKey] whose ID is [id].
   *
   * The [id] may be of arbitrary length (this will be fixed in the
   * future), and may start with an optional `0x`.
   * The presence (or not) of the leading `0x` sets [hasHexPrefix].
   */
  OpenPgpKey(String id) {
    // Trim all spaces in [id]
    id = id.replaceAll(new RegExp('\\s'), '');

    // Extract the optional `0x` prefix
    if (id.startsWith(_HEX_PREFIX)) {
      _hexPrefix = _HEX_PREFIX;
      _id = id.substring(_HEX_PREFIX.length);
    } else {
      _hexPrefix = '';
      _id = id;
    }

    // We can't even extract a shortId with a key ID of less than 8 chars
    if (_id.length < _BITS_SHORTID) {
      throw new ArgumentError('$_id is less than 8 hexadecimal characters.');
    }
    // Only accept lengths of shortIds, longIds, and fingerprints. FIXME?
    if (!_VALID_ID_SIZES.contains(_id.length)) {
      throw new ArgumentError("$_id doesn't have the length of a key ID.");
    } else {
      shortId = _id.substring(_id.length - _BITS_SHORTID, _id.length);
      fingerprint = longId = shortId; // FIXME

      if (_id.length >= _BITS_LONGID) {
        longId = _id.substring(_id.length - _BITS_LONGID, _id.length);
      }
      if (_id.length == _BITS_FINGERPRINT) {
        fingerprint = _id.substring(_id.length - _BITS_FINGERPRINT, _id.length);
      }
    }
  }

  /**
   * Indicates whether identifiers are prefixed with `0x`.
   */
  bool get hasHexPrefix => _hexPrefix == _HEX_PREFIX;
       set hasHexPrefix(bool value) {
         _hexPrefix = value ? _HEX_PREFIX : '';
       }

  String get shortId0x     => _HEX_PREFIX + shortId;
  String get longId0x      => _HEX_PREFIX + longId;
  String get fingerprint0x => _HEX_PREFIX + fingerprint;

  // A bare call to [new OpenPgpKey] will just initialize it.
  call(String id) => new OpenPgpKey(id);

  /**
   * Returns a string representation of an [OpenPgpKey].
   */
  String toString() {
    var buf = new StringBuffer()
        ..writeln("Prefix:      '$_hexPrefix'")
        ..writeln("shortId:     '$shortId'")
        ..writeln("longId:      '$longId'")
        ..writeln("fingerprint: '$fingerprint'");
    return (buf.toString());
  }
}