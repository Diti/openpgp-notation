import 'package:openpgp_notation/dt_openpgp.dart';
import 'package:args/args.dart';

/*
 * Under normal use, it is believed that --signer will almost always be the
 * same key. So go ahead and change this with YOUR public key's fingerprint.
 *
 * Spaces are ignored: `FD4F 1D56 6452 19A0 C6F6  F9AB 31A4 9121 CD42 FF00`
 * would work too. Also, prefixing your key with `0x` (the hexadecimal prefix)
 * works too.
 */
const String DEFAULT_KEY = 'FD4F1D56645219A0C6F6F9AB 31A49121 CD42FF00';

main(List<String> args) {
  var parser = new ArgParser()
      ..addOption('signer', abbr: 'a', defaultsTo: DEFAULT_KEY,
          help: 'The key ID (or fingerprint) of the person certifying the key')
      ..addOption('signee', abbr: 'b',
          help: 'The key ID (or fingerprint) of the person being certified')
      ..addOption('date', abbr: 'd',
          help: 'By default, the current day (YYYY-MM-DD) is used.')
      ..addOption('location', abbr: 'l',
          help: 'The location where the key certification was performed.')
      ..addOption('output', abbr: 'o',
          help: 'If set, writes output to this file instead.');

  var argResults = parser.parse(args);
  var argSigner = argResults['signer'];
  var argSignee = argResults['signee'];
  var argDate = argResults['date'];
  var argLocation = argResults['location'];
  var argOutput = argResults['output'];

  if (args == null || argSignee == null) {
    print('\n' + parser.getUsage());
  }

  if (argSigner != null && argSignee != null) {
    OpenPgpKey signer = new OpenPgpKey(argSigner);
    OpenPgpKey signee = new OpenPgpKey(argSignee);

    // TODO: How about a OpenPgpNotation.fromArgResults() named constructor?
    var notation = new OpenPgpNotation(signer, signee,
        date: argDate, location: argLocation);
    notation.toFile(argOutput);
  } else {
    print('\nNo OpenPGP notation file created.');
    print('Perhaps one of the IDs you provided is invalid?');
    print('- Signer: ${argSigner}');
    print('- Signee: ${argSignee}');
  }
}