openpgp-notation
================

*OpenPGP Notation* is a simple Dart command-line utility for generating [OpenPGP notation][RFC 4880 - notations] files.

With GnuPG, you may provide a notation with option `--cert-notation`, like so:

    gpg --cert-notation CD42FF00@diti.me=http://diti.me/pgp/certs/%f.notes.asc --ask-cert-level --lsign-key 2C6464AF2A8E4C02

The aim of *OpenPGP Notation* is to make the key certification process faster, by automating the redaction of such files.

Pitfalls
--------

Currently, this utility is very basic; Non-Dart programmers may have issues using this utility:

  - My personnal details are hardcoded (search all occurences of `Diti` and `CD42FF00` in the code before using this beta).
  - The output file's contents are hardcoded; by the way, the `--signer` option has no real effect yet.
  - Fingerprint output is not pretty (no spacing). This is why I prefered to include my own prettified fingerprint
  instead of a `GIBBERISHLYUNSPACEDFINGERPRINT`

More generally, consider that **this utility is not yet ready for production** (there is no `master` branch yet).

Example
-------

    $ dart openpgp-notation.dart # Prints usage

    $ dart openpgp-notation.dart \
	--signer "FD4F 1D56 6452 19A0 C6F6  F9AB 31A4 9121 CD42 FF00" \
	--signee "6781 9B34 3B2A B70D ED93  2087 2C64 64AF 2A8E 4C02" \
	--location "Online"

    $ cat out/67819B343B2AB70DED9320872C6464AF2A8E4C02.notes

License
-------

*OpenPGP Notation* is released under the terms of the LGPLv3, so that any modification you make helps me improve.
For a different, more permissive license (MIT), please contact me.


[RFC 4880 - notations]: https://tools.ietf.org/html/rfc4880#section-5.2.3.16
